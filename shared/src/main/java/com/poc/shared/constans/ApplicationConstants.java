package com.poc.shared.constans;

import java.util.Arrays;
import java.util.List;

public class ApplicationConstants {
    public static final String SPRING_PROFILE_DEVELOPMENT = "dev";
    public static final String SPRING_PROFILE_TEST = "test";
    public static final String SPRING_PROFILE_PRODUCTION = "prod";

    public static final String SPRING_PROFILE_DEFAULT = "spring.profiles.default";

    public static final String DEFAULT_SWAGGER_URL = "/v2/api-docs";
    public static final String KEY_SWAGGER_URL = "swagger_url";

    public static final String ZONE_ID = "Europe/Istanbul";

    public static final List<String> EXCEPT_LOG_METHODS = Arrays.asList("info");

    private ApplicationConstants() {
        throw new IllegalStateException("Utility class");
    }
}

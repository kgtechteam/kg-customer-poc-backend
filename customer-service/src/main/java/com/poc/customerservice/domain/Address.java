package com.poc.customerservice.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@EqualsAndHashCode
public class Address implements Serializable {
    private static final long serialVersionUID = 4862538568656232671L;

    private String country;
    private String city;
    private String street;
    private Long postCode;
}

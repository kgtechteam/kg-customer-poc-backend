package com.poc.customerservice.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.junit.Assert;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;

@Data
@EqualsAndHashCode
@ToString
@Document(collection = "customer")
public class Customer implements Serializable {
    private static final long serialVersionUID = -2895683599294993741L;

    @Id
    @Positive
    private Long tckn;

    @NotBlank
    @NotNull
    private String name;

    @NotBlank
    @NotNull
    private String surname;

    @NotNull
    private String phone;

    @Positive
    private Integer birthYear;

    @Email
    @NotNull
    private String email;

    @NotNull
    private Address address;

    public void update(Customer customer) {
        Assert.assertNotEquals("Customer already up to date", this, customer);
        this.setEmail(customer.getEmail());
        this.setPhone(customer.getPhone());
        this.setAddress(customer.getAddress());
    }
}

package com.poc.customerservice.service.impl;

import com.poc.customerservice.domain.Customer;
import com.poc.customerservice.domain.QCustomer;
import com.poc.customerservice.dto.CustomerUpdateResponse;
import com.poc.customerservice.repository.CustomerRepository;
import com.poc.customerservice.service.CustomerService;
import com.poc.shared.exception.AlreadyExistException;
import com.querydsl.core.types.Predicate;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames = "customer")
public class CustomerServiceImpl implements CustomerService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private CustomerRepository repository;

    public CustomerServiceImpl(CustomerRepository repository) {
        this.repository = repository;
    }

    @Override
    @Cacheable
    public PageImpl<Customer> allCustomers(Predicate predicate, int page, int size) {
        if (predicate == null) {
            predicate = QCustomer.customer.tckn.ne(0L);
        }
        PageRequest pageRequest = PageRequest.of(page, size);
        Page<Customer> customerPage = repository.findAll(predicate, pageRequest);
        return new PageImpl<>(customerPage.getContent(), pageRequest, repository.count());
    }

    @Override
    @CacheEvict(allEntries = true)
    public Customer save(Customer customer) throws AlreadyExistException {
        if (!repository.existsById(customer.getTckn())) {
            return repository.save(customer);
        } else {
            throw new AlreadyExistException("Customer already exist");
        }
    }

    @Override
    @CacheEvict(allEntries = true)
    public CustomerUpdateResponse update(Customer customer) {
        Customer willBeUpdatedCustomer = repository.findById(customer.getTckn()).orElse(null);
        Assert.assertNotNull("Customer not found !", willBeUpdatedCustomer);
        CustomerUpdateResponse response = new CustomerUpdateResponse(willBeUpdatedCustomer);

        willBeUpdatedCustomer.update(customer);
        willBeUpdatedCustomer = repository.save(willBeUpdatedCustomer);
        response.setNewCustomer(willBeUpdatedCustomer);
        return response;
    }


}

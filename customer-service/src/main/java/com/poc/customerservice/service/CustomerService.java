package com.poc.customerservice.service;

import com.poc.customerservice.domain.Customer;
import com.poc.customerservice.dto.CustomerUpdateResponse;
import com.poc.shared.exception.AlreadyExistException;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.PageImpl;


public interface CustomerService {
    PageImpl<Customer> allCustomers(Predicate predicate, int page, int size);

    Customer save(Customer customer) throws AlreadyExistException;

    CustomerUpdateResponse update(Customer customer);
}

package com.poc.customerservice.dto;

import com.poc.customerservice.domain.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerUpdateResponse {
    private Customer oldCustomer;
    private Customer newCustomer;

    public CustomerUpdateResponse(Customer oldCustomer) {
        this.oldCustomer = oldCustomer;
    }
}

package com.poc.customerservice.repository;

import com.poc.customerservice.domain.Customer;
import com.poc.customerservice.domain.QCustomer;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.core.types.dsl.StringPath;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;

public interface CustomerRepository extends MongoRepository<Customer, Long>, QuerydslPredicateExecutor<Customer>, QuerydslBinderCustomizer<QCustomer> {
    @Override
    default void customize(QuerydslBindings querydslBindings, QCustomer customer) {
        querydslBindings.bind(customer.tckn).first(NumberExpression::eq);
        querydslBindings.bind(customer.name).first(StringExpression::containsIgnoreCase);
        querydslBindings.bind(customer.surname).first(StringExpression::containsIgnoreCase);
        querydslBindings.bind(customer.email).first(StringExpression::containsIgnoreCase);
        querydslBindings.bind(customer.phone).first(StringExpression::containsIgnoreCase);
        querydslBindings.bind(customer.birthYear).first(NumberExpression::eq);
        querydslBindings.bind(customer.address.city).first(StringExpression::containsIgnoreCase);
        querydslBindings.bind(customer.address.country).first(StringExpression::containsIgnoreCase);
        querydslBindings.bind(customer.address.postCode).first(NumberExpression::eq);
        querydslBindings.bind(customer.address.street).first(StringExpression::containsIgnoreCase);
        querydslBindings.bind(String.class).first(
                (SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}

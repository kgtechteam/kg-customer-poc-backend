package com.poc.customerservice.controller;

import com.poc.customerservice.domain.Customer;
import com.poc.customerservice.dto.CustomerUpdateResponse;
import com.poc.customerservice.service.CustomerService;
import com.poc.shared.exception.AlreadyExistException;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = CustomerController.END_POINT)
public class CustomerController {
    static final String END_POINT = "/api/customer";

    private CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping
    public PageImpl<Customer> customers(@QuerydslPredicate(root = Customer.class) Predicate predicate,
                                        @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                        @RequestParam(value = "size", required = false, defaultValue = "10") int size) {
        return this.customerService.allCustomers(predicate, page, size);
    }

    @PostMapping
    public Customer save(@RequestBody @Valid Customer customer) throws AlreadyExistException {
        return this.customerService.save(customer);
    }

    @PutMapping
    public CustomerUpdateResponse update(@RequestBody @Valid Customer customer) {
        return this.customerService.update(customer);
    }
}

package com.poc.loggerservice.service;

import com.poc.loggerservice.domain.Log;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;


public interface LogService {
    void save(Log log);

    PageImpl<Log> findAll(Predicate predicate, int size, int page);
}

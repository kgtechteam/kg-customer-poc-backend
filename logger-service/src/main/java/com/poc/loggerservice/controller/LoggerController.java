package com.poc.loggerservice.controller;

import com.poc.loggerservice.domain.Log;
import com.poc.loggerservice.service.LogService;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(LoggerController.END_POINT)
public class LoggerController {
    static final String END_POINT = "/api/logs";

    private LogService logService;

    public LoggerController(LogService logService) {
        this.logService = logService;
    }

    @GetMapping
    public PageImpl<Log> help(@QuerydslPredicate(root = Log.class) Predicate predicate,
                         @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                         @RequestParam(value = "size", required = false, defaultValue = "10") int size) {
        return logService.findAll(predicate, page, size);
    }
}
